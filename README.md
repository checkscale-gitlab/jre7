# Introduction

This project creates a docker image with JRE 7 installed installed on Debian Jessie.

The image can be used to run applications using java.

This repository is mirrored to https://gitlab.com/sw4j-net/jre7

## Deprecation

As Java 7 is deprecated this repository and the generated images will be removed in January 2019.
